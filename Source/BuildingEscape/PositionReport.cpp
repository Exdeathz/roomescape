// Fill out your copyright notice in the Description page of Project Settings.

#include "BuildingEscape.h"
#include "PositionReport.h"
/*
Be sure to know about the include what you use mode for Unreal engine 4.17 and up.
https://docs.unrealengine.com/en-us/Programming/UnrealBuildSystem/IWYUReferenceGuide
*/


// Sets default values for this component's properties
UPositionReport::UPositionReport()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
	
}


// Called when the game starts
void UPositionReport::BeginPlay()
{
	Super::BeginPlay();

	// ...
	// AActor* owner = GetOwner(); Get owner returns a pointer to an actor object.
	FString ObjectPos = GetOwner()->GetActorTransform().GetLocation().ToString();
	FString myname = GetOwner()->GetName();



	UE_LOG(LogTemp, Warning, TEXT("%s is at %s"), *myname, *ObjectPos);
}


// Called every frame
void UPositionReport::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	// ...
}

