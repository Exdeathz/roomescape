// Copyright Jared Long 2018

#include "BuildingEscape.h"
#include "OpenDoor.h"


// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();
	///ActorThatOpens = GetWorld()->GetFirstPlayerController()->GetPawn();
	BaseRotation = GetOwner()->GetActorRotation();
	// ...
	//OpenDoor();
}

void UOpenDoor::OpenDoor()
{
	// Find the owning actor.
	//AActor * Owner = GetOwner();
	//FRotator Rotation = BaseRotation;
	//Rotation.Add(0.0f, OpenAngle, 0.0f);
	//Owner->AddActorLocalRotation(Rotation);
	//Owner->SetActorRotation(Rotation);
	OnOpenRequest.Broadcast();
}

void UOpenDoor::CloseDoor()
{
	// Find the owning actor.
	//AActor * Owner = GetOwner();
	//FRotator Rotation = BaseRotation;
	//Owner->AddActorLocalRotation(Rotation);
	//Owner->SetActorRotation(Rotation);

	OnCloseRequest.Broadcast();
}


// Called every frame
void UOpenDoor::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	// ...
	//Poll the Trigger Volume every frame
	if (GetTotalMassOnPlate() >= TriggerMass)
	{
		OpenDoor();
		LastDoorOpenTime = GetWorld()->GetTimeSeconds();
	}
	//if the actor that opens the door is in the volume, then open the door.
	//Check if it is time to close the door.
	if ((LastDoorOpenTime + DoorCloseDelay) <= GetWorld()->GetTimeSeconds())
	{
		CloseDoor();
	}
}

float UOpenDoor::GetTotalMassOnPlate()
{
	TArray<AActor*> ActorsOnPlate;
	float totalmass = 0.0f;
	if (PressurePlate)
	{
		PressurePlate->GetOverlappingActors(ActorsOnPlate);
		for (AActor *Actor : ActorsOnPlate)
		{
			totalmass = Actor->FindComponentByClass<UPrimitiveComponent>()->GetMass() + totalmass;
		}
	}
	
	///UE_LOG(LogTemp, Warning, TEXT("Mass calculated as %s"), *FString::FromInt(totalmass)); this has an example of converting a float to an fstring.
	return totalmass;
}

