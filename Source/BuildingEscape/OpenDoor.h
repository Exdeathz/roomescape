// Copyright Jared Long 2018

#pragma once

#include "Components/ActorComponent.h"
#include "OpenDoor.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDoorEvent);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UOpenDoor : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOpenDoor();

	// Called when the game starts
	virtual void BeginPlay() override;

	void OpenDoor();
	void CloseDoor();
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	UPROPERTY(BlueprintAssignable)
	FDoorEvent OnOpenRequest;

	UPROPERTY(BlueprintAssignable)
	FDoorEvent OnCloseRequest;

		
private:
	UPROPERTY(EditAnywhere)
	float OpenAngle = -90.f;
	UPROPERTY(EditAnywhere)
	ATriggerVolume * PressurePlate = nullptr;
	///AActor * ActorThatOpens;//remember pawn inherits from actor
	UPROPERTY(EditAnywhere)
	float DoorCloseDelay = 1.f;
	float LastDoorOpenTime;
	FRotator BaseRotation;

	UPROPERTY(EditAnywhere)
	float TriggerMass;

	//returns total mass in Kg.
	float GetTotalMassOnPlate();
};
