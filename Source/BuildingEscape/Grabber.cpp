// Copyright Jared Long 2018

#include "BuildingEscape.h"
#include "Grabber.h"

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();
	myPlayerController = GetWorld()->GetFirstPlayerController();
	FindPhysicsHandleComponent();
	SetupControls();

	// ...
	
}

// Called every frame
void UGrabber::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	// Get the end of our Grabber reach and move any held object to that location.
	FVector LineTraceEnd = GetLineTraceEnd();
	if (PhysicsHandle) ///protect against calls to null pointer
	{
		if (PhysicsHandle->GrabbedComponent)
		{
			PhysicsHandle->SetTargetLocation(LineTraceEnd);
		}
	}
}

void UGrabber::Grab()
{
	///ray-cast and grab.
	UE_LOG(LogTemp, Display, TEXT("Grab Pressed"));
	auto HitResult = GetFirstPhysicsBodyInReach();
	auto ComponentToGrab = HitResult.GetComponent();
	auto ActorHit = HitResult.GetActor();
	if (ActorHit)
	{
		if (PhysicsHandle) ///protect against calls to null pointer
		{
			PhysicsHandle->GrabComponent(
				ComponentToGrab,
				NAME_None, //no bones needed
				ComponentToGrab->GetOwner()->GetActorLocation(),
				true //allow rotation
			);
		}
	}
}

void UGrabber::Release()
{
	///ray-cast and grab.
	UE_LOG(LogTemp, Display, TEXT("Grab released"));
	if (PhysicsHandle)///protect against calling a null pointer
	{
		PhysicsHandle->ReleaseComponent();
	}
}

void UGrabber::FindPhysicsHandleComponent()
{
	/// Look for attached physics handle.
	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	if (!PhysicsHandle)
	{

		UE_LOG(LogTemp, Error, TEXT("Phisics Handle is null on object: %s"), *GetOwner()->GetName());
	}
}

void UGrabber::SetupControls()
{
	/// Look for attached input component
	InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();
	if (!InputComponent)
	{

		UE_LOG(LogTemp, Error, TEXT("Input Component is null on object: %s"), *GetOwner()->GetName());
	}
	else
	{
		///bind the input axis.
		InputComponent->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
		InputComponent->BindAction("Grab", IE_Released, this, &UGrabber::Release);
	}
}

FHitResult UGrabber::GetFirstPhysicsBodyInReach()
{
	if (myPlayerController)///protect against null pointer for the player controller.
	{
		FVector location; //create a temp out variable.
		FRotator rotation; //also a temporary out variable
		myPlayerController->GetPlayerViewPoint(location, rotation);
		FVector LineTraceEnd = GetLineTraceEnd();

		DrawDebugLine(GetWorld(), location, LineTraceEnd, FColor(255, 0, 0), false, 0.f, 0.f, 10.0f);

		///Setup Query Parameters
		FCollisionQueryParams TraceParameters(FName(TEXT("")), false, GetOwner());
		/// Line-trace aka Ray-cast out to reach distance
		FHitResult Hit;

		GetWorld()->LineTraceSingleByObjectType(
			Hit,
			location,
			LineTraceEnd,
			FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
			TraceParameters
		);

		AActor * ActorHit = Hit.GetActor();
		if (ActorHit)
		{
			UE_LOG(LogTemp, Warning, TEXT("Hit Actor: %s"), *ActorHit->GetName());
		}
		return Hit;
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Actor does not have a playercontroller set before using grabber: %s"), *GetOwner()->GetName())
		return FHitResult();
	}
}

FVector UGrabber::GetLineTraceEnd()
{
	if (myPlayerController)///protect against null pointer use
	{
		// Get the player viewpoint this tick
		FVector location; //create a temp out variable.
		FRotator rotation; //also a temporary out variable

		myPlayerController->GetPlayerViewPoint(location, rotation);

		FString locs = location.ToString(); //convert the values to strings that we can output.
		FString rots = rotation.ToString(); //convert this to string as well.
											//UE_LOG(LogTemp, Warning, TEXT("Viewpoint Location: %s  Viewpoint Rotation: %s"), *locs, *rots);

											//Draw a line.
		FVector LineTraceEnd = location + (rotation.Vector()*Reach);
		return LineTraceEnd;
	}
	else
	{
		return FVector(0.f, 0.f, 0.f);
	}
}
