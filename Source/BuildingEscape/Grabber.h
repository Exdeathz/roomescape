// Copyright Jared Long 2018

#pragma once

#include "Components/ActorComponent.h"
#include "Grabber.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

private:
	APlayerController * myPlayerController = nullptr;
	UPROPERTY(EditAnywhere)
	float Reach = 100.f;

	UPhysicsHandleComponent * PhysicsHandle = nullptr;

	UInputComponent * InputComponent = nullptr;


	//Ray cast, and grab what is in reach.
	void Grab();
	void Release();

	void FindPhysicsHandleComponent(); //verifies that a Physics handle component exists. Grabber doesn't work witout one.
	void SetupControls(); //Sets up the contols that are needed to trigger the grabber.
	FHitResult GetFirstPhysicsBodyInReach();
	FVector GetLineTraceEnd();
	
};
